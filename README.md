![promiseQ-Task](./banner.png)

# promiseQ-Task

promiseQ is no ordinary video analytics security provider. We offer a unique blend of cutting-edge AI and certified human review through a global platform, ensuring top-notch security solutions with a personal touch.

[Website](https://total-pad-406218.web.app/) | [Demo](link)
