FROM python:3.10-slim

WORKDIR /app

# Copy only the requirements file to the /app directory in the container
COPY fastapi/requirements.txt /app

# Install the Python dependencies
RUN pip install -r /app/requirements.txt

# Copy the rest of the files (excluding Docker ignore files)
COPY . .

CMD cd fastapi && uvicorn main:app --port=8000 --host=0.0.0.0