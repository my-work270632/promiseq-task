
#  by sahanr.silva@proton.me

from fastapi import FastAPI, File, UploadFile, Form, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from os import environ as env
import requests
import json

app = FastAPI()

# Adding CORS middleware to allow cross-origin requests.
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Endpoint to handle POST requests for sending image data and box coordinates to threat detection service.
@app.post("/")
async def send_request_to_threat_detect(boxCoordinates: str = Form(...), image: UploadFile = File(...)):
    try:
        # Convert boxCoordinates string to a list of numerical coordinates.
        coordinates_list = json.loads(boxCoordinates)

        # URL of the threat detection service.
        url = 'https://europe-west1-promiseq-production2.cloudfunctions.net'

        # Setting headers for the request including subscription key for authentication.
        headers = {
            'promiseq-subscription-key': env['key']
        }

        # Prepare data as a JSON payload containing box coordinates.
        payload = {
            'boxCoordinates': coordinates_list
        }

        # Prepare files data to send the image along with the request.
        files = {'image': (image.filename, image.file)}

        # Sending a POST request to the threat detection service.
        response = requests.post(url, headers=headers, data=payload, files=files)
        response.raise_for_status()  # Raise an error for 4xx or 5xx status codes

        # Return the JSON response from the threat detection service.
        return response.json()
    except (requests.RequestException, json.JSONDecodeError) as e:
        # Raise an HTTP Exception if there's an issue with the request or response.
        raise HTTPException(status_code=500, detail=str(e))
